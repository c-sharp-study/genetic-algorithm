﻿using GeneticAlgorithm.Algorithm;
using GeneticAlgorithm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeneticAlgorithm
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            TspGA ga = new TspGA(100, 20, 0.80, 0.05, new TSPGAOperation(), 20);
            TspGaViewModel tspGaViewModel = new TspGaViewModel(ga);
            //GeneticAlgorithmViewModel gaViewModel = new GeneticAlgorithmViewModel(ga);
            DataContext = tspGaViewModel;
            //ga.Execute();
            //MessageBox.Show((1.0 / ga.BestChromosome.Fitness).ToString());
            // MessageBox.Show(gaViewModel.Population.Count.ToString());
        }

    }
}
