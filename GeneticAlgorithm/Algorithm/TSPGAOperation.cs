﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Algorithm
{
    /// <summary>
    /// TSP问题的遗传算法染色体操作，实现了遗传算法操作接口
    /// </summary>
    class TSPGAOperation : IGAOperation<int>
    {

        /// <summary>
        /// TSP问题的交叉操作，采用次序交叉法，保证交叉后的染色体基因位不重复
        /// </summary>
        /// <param name="crossProb">交叉概率</param>
        /// <param name="father">父染色体</param>
        /// <param name="mother">母染色体</param>
        /// <returns>交叉后的子染色体</returns>
        public Chromosome<int> Crossover(double crossProb, Chromosome<int> father, Chromosome<int> mother)
        {
            // 初始化子染色体的基因位列表
            List<int> child = new List<int>();

            // 判断是否需要交叉
            var rand = new Random();
            if (rand.NextDouble() < crossProb)
            {
                // 先从父染色体中挑选基因位
                foreach(int city in father.Data)
                {
                    if (rand.NextDouble() < 0.6)
                    {
                        child.Add(city);
                    } else
                    {
                        child.Add(-1);
                    }
                }

                // 从母染色体中填补剩余的基因位
                foreach(int city in mother.Data)
                {
                    if (!child.Contains(city))
                    {
                        for (int i = 0; i < child.Count; i++)
                        {
                            if(child[i] == -1)
                            {
                                child[i] = city;
                                break;
                            }
                        }
                    }
                }

                return new Chromosome<int>(child);
            }
            return father;
        }

        /// <summary>
        /// TSP问题的变异操作， 任意选择两个基因位，并交换两者的位置
        /// </summary>
        /// <param name="mutateProb">变异概率</param>
        /// <param name="chromosome">需要变异的染色体</param>
        /// <returns>变异后的染色体</returns>
        public Chromosome<int> Mutate(double mutateProb, Chromosome<int> chromosome)
        {
            var rand = new Random();

            if (rand.NextDouble() < mutateProb)
            {
                int a = rand.Next(0, chromosome.Data.Count - 1);
                int b;
                do
                {
                    b = rand.Next(0, chromosome.Data.Count - 1);

                }
                while (b == a);
               
                List<int> child = new List<int>(chromosome.Data);
                var temp = child[a];
                child[a] = child[b];
                child[b] = temp;
                return new Chromosome<int>(child);
            }
            return chromosome;
        }


        /// <summary>
        /// TSP问题选择操作
        /// </summary>
        /// <param name="population">旧种群</param>
        /// <returns>新种群</returns>
        public List<Chromosome<int>> Select(List<Chromosome<int>> population)
        {
            // 初始化新种群
            var newPopulation = new List<Chromosome<int>>();

            // 计算每条染色体的累计概率
            List<double> accumulativeProb = new List<double>();
            double totalFitness = population.Sum(c => c.Fitness);
            double prob = 0.0;
            foreach (Chromosome<int> chromosome in population)
            {
                prob = prob + chromosome.Fitness / totalFitness;
                accumulativeProb.Add(prob);
            }

            // 轮盘赌进行选择
            var rand = new Random();
            foreach (Chromosome<int> c in population)
            {
                int idx = 0;
                var randNum = rand.NextDouble();
                for (int i = 0; i < accumulativeProb.Count; i++)
                {
                    if (randNum > accumulativeProb[i])
                    {
                        idx++;
                    }
                }
                newPopulation.Add(new Chromosome<int>(new List<int>(population[idx].Data)));

            }
            return newPopulation;
        }
    }


}
