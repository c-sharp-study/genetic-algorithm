﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GeneticAlgorithm.Algorithm
{
    /// <summary>
    /// TSP问题的遗传算法
    /// </summary>
    class TspGA : GeneticAlgorithm<int>
    {
        /// <summary>
        /// 城市坐标点列表
        /// </summary>
        public List<Point> Cities { get; set; }

        /// <summary>
        /// 距离矩阵二维数组
        /// </summary>
        private double[,] distanceMat;

        /// <summary>
        /// TSP问题遗传算法构造函数
        /// </summary>
        /// <param name="numTrial">最大迭代次数</param>
        /// <param name="maxPopulation">种群规模</param>
        /// <param name="crossoverProb">交叉概率</param>
        /// <param name="mutateProb">变异概率</param>
        /// <param name="gaOperation">TSP遗传算法的操作</param>
        public TspGA(int numTrial, int maxPopulation, double crossoverProb, double mutateProb, IGAOperation<int> gaOperation, int numCity) : base(numTrial, maxPopulation, crossoverProb, mutateProb, gaOperation)
        {
            var rand = new Random();
            // 根据城市数量随机初始化城市坐标
            Cities = new List<Point>();
            for (int i = 0; i < numCity; i++)
            {
                Cities.Add(new Point(rand.NextDouble() * 300, rand.NextDouble() * 300));
            }
            // 计算距离矩阵
            distanceMat = new double[Cities.Count, Cities.Count];
            for (int i =0; i < Cities.Count; i++)
            {
                for (int j = 0; j < Cities.Count; j++)
                {
                    var distance = ComputeDistance(Cities[i], Cities[j]);
                    distanceMat[i, j] = distance;
                    distanceMat[j, i] = distance;
                }
            }
        }

        /// <summary>
        /// TSP问题初始化种群
        /// </summary>
        /// <returns>初始种群</returns>
        public override List<Chromosome<int>> InitPopulation()
        {
            // 初始种群列表
            var population = new List<Chromosome<int>>();

            // 为了构造不含重复值得基因位，需先初始化一个临时的数组，包含0-N这些数字
            var rand = new Random();
            int[] tempArray = new int[Cities.Count];
            for (int i = 0; i < Cities.Count; i++)
            {
                tempArray[i] = i;
            }

            // 初始化种群
            for (int i = 0; i < MaxPopulation; i++)
            {

                // 初始化染色体的基因位数据
                List<int> data = new List<int>();

                // 随机生成不含重复数据的基因位数据
                for (int j = 0; j < Cities.Count; j++)
                {
                    var randNum = rand.Next(0, Cities.Count - j - 1);
                    data.Add(tempArray[randNum]);
                    var temp = tempArray[Cities.Count - 1 - j];
                    tempArray[Cities.Count - 1 - j] = tempArray[randNum];
                    tempArray[randNum] = temp;
                }
                // 将随机生成的染色体添加到种群列表中
                population.Add(new Chromosome<int>(data));
            }
            return population;
        }

        /// <summary>
        /// TSP问题计算染色体的适应度值，其适应度值为总距离的倒数
        /// </summary>
        /// <param name="chromosome">染色体</param>
        /// <returns>计算好适应度值得染色体</returns>
        public override Chromosome<int> UpdateFitness(Chromosome<int> chromosome)
        {
            double distance = 0.0;
            for (int i = 0; i < Cities.Count - 1; i++)
            {
                distance += ComputeDistance(Cities[chromosome.Data[i]], Cities[chromosome.Data[i + 1]]);
            }
            distance += ComputeDistance(Cities[chromosome.Data[Cities.Count - 1]], Cities[chromosome.Data[0]]);
            chromosome.Fitness = 1.0 / distance;
            return chromosome;
        }

        /// <summary>
        /// 辅助函数，计算任意两点之间的距离
        /// </summary>
        /// <param name="p1">点1</param>
        /// <param name="p2">点2</param>
        /// <returns>距离</returns>
        private double ComputeDistance(Point p1, Point p2)
        {
            double distance = (p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y);
            distance = Math.Sqrt(distance);
            return distance;
        }
    }
}
