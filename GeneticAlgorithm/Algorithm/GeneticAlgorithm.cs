﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Algorithm
{
    /// <summary>
    /// 遗传算法的抽象类，继承该抽象类需实现计算适应度值<see cref="UpdateFitness"/>以及初始化种群<see cref="InitPopulation"/>这两个抽象方法
    /// </summary>
    /// <typeparam name="T">染色体基因位的类型</typeparam>
    abstract class GeneticAlgorithm<T>
    {
        /// <summary>
        /// 最大迭代次数
        /// </summary>
        public int NumTrial { get; set; }

        /// <summary>
        /// 种群规模
        /// </summary>
        public int MaxPopulation { get; set; }

        /// <summary>
        /// 变异概率
        /// </summary>
        public double MutateProb { get; set; }

        /// <summary>
        /// 交叉概率
        /// </summary>
        public double CrossoverProb { get; set; }

        /// <summary>
        /// 种群，即染色体列表
        /// </summary>
        public List<Chromosome<T>> Population { get; set; }

        /// <summary>
        /// 遗传算法的3大操作
        /// </summary>
        public IGAOperation<T> GAOperation { get; private set; }

        /// <summary>
        /// 最优染色体
        /// </summary>
        public Chromosome<T> BestChromosome { get; private set; }

        /// <summary>
        /// 遗传算法类的构造函数
        /// </summary>
        /// <param name="numTrial">最大迭代次数</param>
        /// <param name="maxPopulation">种群规模</param>
        /// <param name="crossoverProb">交叉概率</param>
        /// <param name="mutateProb">变异概率</param>
        /// <param name="gaOperation">遗传算法的操作</param>
        public GeneticAlgorithm(int numTrial, int maxPopulation, double crossoverProb, double mutateProb, IGAOperation<T> gaOperation)
        {
            NumTrial = numTrial;
            MaxPopulation = maxPopulation;
            MutateProb = mutateProb;
            CrossoverProb = crossoverProb;
            GAOperation = gaOperation;
        }

        public abstract List<Chromosome<T>> InitPopulation();

        public abstract Chromosome<T> UpdateFitness(Chromosome<T> population);

        public void Execute()
        {
            var rand = new Random();
            Population = InitPopulation();
            
            for (int i = 0; i <= NumTrial; i++)
            {
                // 计算种群内每条染色体的适应度值
                Population = UpdateAllFitness(Population);
                // 执行遗传算法选择操作
                Population = GAOperation.Select(Population);
                // 对种群内的每条染色体执行遗传算法的交叉操作
                var motherIdx = rand.Next(0, Population.Count - 1);
                var mother = Population[motherIdx];
                Population = Population.Select(father => GAOperation.Crossover(this.CrossoverProb, father, mother)).ToList();
                // 对种群内的每条染色体执行遗传算法的变异操作
                Population = Population.Select(c => GAOperation.Mutate(this.MutateProb, c)).ToList();
            }
        }

        public List<Chromosome<T>> UpdateAllFitness(List<Chromosome<T>> population)
        {
            var updatedPopulation =  population.Select(c => UpdateFitness(c)).ToList();
            // 记录该种群中最优染色体
            var bestFitness = Population.Max(c => c.Fitness);
            //BestChromosome = Population.Where(c => c.Fitness == bestFitness).First();
            if (null == BestChromosome || bestFitness > BestChromosome.Fitness)
            {
                BestChromosome = Population.Where(c => c.Fitness == bestFitness).First();
            }

            return updatedPopulation;
        }

    }
}
