﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Algorithm
{
    class Chromosome<T>
    {
        public Chromosome(List<T> data)
        {
            Data = data;
        }
        public List<T> Data { get; set; }
        public double Fitness { get; set; }
    }
}
