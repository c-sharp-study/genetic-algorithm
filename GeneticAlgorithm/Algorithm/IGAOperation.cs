﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Algorithm
{
    /// <summary>
    /// 遗传算法3大主要操作的接口
    /// </summary>
    /// <typeparam name="T">染色体基因位的类型</typeparam>
    interface IGAOperation<T>
    {
        /// <summary>
        /// 选择操作，接受一个种群，返回一个新的种群
        /// </summary>
        /// <param name="population">旧种群</param>
        /// <returns>新种群</returns>
        List<Chromosome<T>> Select(List<Chromosome<T>> population);

        /// <summary>
        /// 交叉操作
        /// </summary>
        /// <param name="crossProb">交叉概率</param>
        /// <param name="father">父染色体</param>
        /// <param name="mother">母染色体</param>
        /// <returns>交叉后的子染色体</returns>
        Chromosome<T> Crossover(double crossProb, Chromosome<T> father, Chromosome<T> mother);

        /// <summary>
        /// 变异操作
        /// </summary>
        /// <param name="mutateProb">变异概率</param>
        /// <param name="chromosome">需要变异的染色体</param>
        /// <returns>变异后的染色体</returns>
        Chromosome<T> Mutate(double mutateProb, Chromosome<T> chromosome);
       
    }
}
