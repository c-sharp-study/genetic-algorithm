﻿using GeneticAlgorithm.Algorithm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace GeneticAlgorithm.ViewModels
{
    class TspGaViewModel : BaseViewModel
    {
        #region 私有字段
        private TspGA ga;
        #endregion

        #region 公有属性
        /// <summary>
        /// 最大迭代次数
        /// </summary>
        public int NumTrial
        {
            get
            {
                return ga.NumTrial;
            }

            set
            {
                ga.NumTrial = value;
            }
        }

        /// <summary>
        /// 种群规模
        /// </summary>
        public int MaxPopulation
        {
            get
            {
                return ga.MaxPopulation;
            }

            set
            {
                ga.MaxPopulation = value;
                // 种群规模发生了变化，重新初始化种群
                ga.Population = ga.InitPopulation();
                // 种群规模发生变化的时候需要通知视图重新刷新
                OnPropertyChanged(nameof(Population));
            }
        }

        /// <summary>
        /// 交叉概率
        /// </summary>
        public double CrossoverProb
        {
            get
            {
                return ga.CrossoverProb;
            }

            set
            {
                ga.CrossoverProb = value;
            }
        }

        /// <summary>
        /// 变异概率
        /// </summary>
        public double MutateProb
        {
            get
            {
                return ga.MutateProb;
            }

            set
            {
                ga.MutateProb = value;
            }
        }

        public int NumCity
        {
            get
            {
                return ga.Cities.Count;
            }
            set
            {
                var rand = new Random();
                var cities = new ObservableCollection<Point>();
                for (int i = 0; i < value; i++)
                {
                    cities.Add(new Point(rand.NextDouble() * 300, rand.NextDouble() * 300));
                }
                Cities = cities;

            }
        }

        /// <summary>
        /// 城市坐标点列表
        /// </summary>
        public ObservableCollection<Point> Cities
        {
            get
            {
                return new ObservableCollection<Point>(ga.Cities);
            }

            set
            {
                ga.Cities = new List<Point>(value);
                // 城市列表发生了变化需要重新初始化种群，并通知试图种群属性发生了变化
                ga.Population = ga.InitPopulation();
                OnPropertyChanged(nameof(Population));
            }
        }


        /// <summary>
        /// 当前最优的路径
        /// </summary>
        public ObservableCollection<Point> BestCitySeq
        {
            get
            {
                // 通过城市编号找到城市坐标点
                // 城市编号 => 坐标点
                // int cityNo => Point point

                // 最傻的方法
                //var points = new ObservableCollection<Point>();
                //var Cities = new List<Point>()
                //{
                //    new Point(0, 0),
                //    new Point(1, 1),
                //    new Point(2, 2),
                //    new Point(3, 3),
                //};

                //var data = new List<int> { 2, 3, 0, 1 };
                //var point0 = Cities[data[0]];
                //var point1 = Cities[data[1]];
                //var point2 = Cities[data[2]];
                //var point3 = Cities[data[3]];
                //points.Add(point0);
                //points.Add(point1);
                //points.Add(point2);
                //points.Add(point3);


                // 好一点的方法1
                //foreach (int cityNo in ga.BestChromosome.Data)
                //{
                //    var point = ga.Cities[cityNo];
                //    points.Add(point);
                //}

                // 好一点的方法2
                //for (int i = 0; i < ga.BestChromosome.Data.Count; i++)
                //{
                //    var cityNo = ga.BestChromosome.Data[i];
                //    var point = ga.Cities[cityNo];
                //}

                if (null == ga.BestChromosome)
                {
                    return new ObservableCollection<Point>();
                }

                // 最简洁的方法
                return new ObservableCollection<Point>(ga.BestChromosome.Data.Select((cityNo) => ga.Cities[cityNo]));
               

            }
        }

        /// <summary>
        /// 种群，即染色体的列表，为了能够在列表数据发生变化的时候主动通知视图刷新，所以列表类的属性应采用<see cref="ObservableCollection{T}"/>,
        /// 因为<see cref="ObservableCollection{T}"/>实现了 <see cref="INotifyPropertyChanged"/> 接口 
        /// </summary>
        public ObservableCollection<ObservableCollection<int>> Population
        {
            get
            {
                if (null == ga.Population)
                {
                    ga.Population = ga.InitPopulation();
                }
                // 将遗传算法类中的Poluation转换成绑定需要的Poplution
                return new ObservableCollection<ObservableCollection<int>>(ga.Population.Select(c => new ObservableCollection<int>(c.Data)));
            }
        }

        /// <summary>
        /// 运行遗传算法程序
        /// </summary>
        public ICommand RunCommand { get; set; }
        #endregion

        #region 构造函数
        /// <summary>
        ///  TSP遗传算法用户界面的ViewModel构造函数
        /// </summary>
        /// <param name="ga">TSP遗传算法类</param>
        public TspGaViewModel(TspGA ga)
        {
            this.ga = ga;
            RunCommand = new RelayCommand(() => ExecuteAsync());
        }
        #endregion

        #region 公有方法
        /// <summary>
        /// 异步执行遗传算法，保证视图能够正常刷新
        /// </summary>
        public void ExecuteAsync()
        {
            Random rand = new Random();
            Task.Run(async () =>
            {
                for (int i = 0; i <= NumTrial; i++)
                {
                    await Task.Delay(10);
                    // 计算种群内每条染色体的适应度值
                    ga.Population = ga.UpdateAllFitness(ga.Population);
                    // 执行遗传算法选择操作
                    ga.Population = ga.GAOperation.Select(ga.Population);
                    // 对种群内的每条染色体执行遗传算法的交叉操作
                    ga.Population = ga.Population.Select(father =>
                    {
                        var motherIdx = rand.Next(0, Population.Count - 1);
                        var mother = ga.Population[motherIdx];
                        return ga.GAOperation.Crossover(this.CrossoverProb, father, mother);
                    }).ToList();
                    // 对种群内的每条染色体执行遗传算法的变异操作
                    ga.Population = ga.Population.Select(c => ga.GAOperation.Mutate(this.MutateProb, c)).ToList();
                    // 通知视图刷新
                    OnPropertyChanged(nameof(Population));
                    OnPropertyChanged(nameof(BestCitySeq));
                }
            });
        }
        #endregion


    }
}
