﻿
using PropertyChanged;
using System.ComponentModel;
using System.Threading.Tasks;

namespace GeneticAlgorithm.ViewModels
{
    [ImplementPropertyChanged]
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, evt) => { };

        public void OnPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

    }
}
